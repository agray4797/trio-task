#!/bin/bash

echo "Enter mysql root password"
read -s password

docker network create trio-task-network
docker volume create trio-volume

docker build -t trio-db db
docker build -t trio-flask-app flask-app

docker run -d --network trio-task-network --mount type=volume,source=trio-volume,target=/var/lib/mysql -e MYSQL_ROOT_PASSWORD=$password --name mysql trio-db
docker run -d --network trio-task-network -e password=$password --name flask-app trio-flask-app
docker run -d --network trio-task-network --mount type=bind,source=$(pwd)/nginx/nginx.conf,target=/etc/nginx/nginx.conf -p 80:80 --name nginx nginx:alpine